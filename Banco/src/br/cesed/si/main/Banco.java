package br.cesed.si.main;

import br.cesed.si.app.Conta;
import br.cesed.si.app.ContaCorrente;

public class Banco {

	public static void main(String[] args) {
		Conta joao = new ContaCorrente(1, 1, "joao");
		joao.depositar(100);
		System.out.println(joao.getSaldo());
	}

}
