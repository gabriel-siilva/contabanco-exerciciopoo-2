/**
 * 6 de mar de 2018
 */
package br.cesed.si.app;

/**
 * @author Gabriel Silva
 *
 */
public final class ContaCorrente extends Conta {
	
	public ContaCorrente(int agencia, int conta, String titular) {
		super(agencia, conta, titular);
	}

}
