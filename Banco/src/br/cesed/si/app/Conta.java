/**
 * 6 de mar de 2018
 */
package br.cesed.si.app;

/**
 * @author Gabriel Silva
 *
 */
public abstract class Conta {

	private int agencia;
	private int conta;
	private String titular;
	private double limite;
	private double saldo;
	private double valorLimite;

	/**
	 * @param agencia
	 * @param conta
	 * @param titular
	 */
	public Conta(int agencia, int conta, String titular) {
		this.agencia = agencia;
		this.conta = conta;
		this.titular = titular;
	}

	/**
	 * @param agencia
	 * @param conta
	 * @param titular
	 * @param saldo
	 */
	public Conta(int agencia, int conta, String titular, double saldo) {
		this(agencia, conta, titular);
		this.saldo = saldo;
	}

	/**
	 * @return the agencia
	 */
	public int getAgencia() {
		return agencia;
	}

	/**
	 * @param agencia
	 *            the agencia to set
	 */
	public void setAgencia(int agencia) {
		this.agencia = agencia;
	}

	/**
	 * @return the conta
	 */
	public int getConta() {
		return conta;
	}

	/**
	 * @return the titular
	 */
	public String getTitular() {
		return titular;
	}

	/**
	 * @param titular
	 *            the titular to set
	 */
	public void setTitular(String titular) {
		this.titular = titular;
	}

	/**
	 * @return the limite
	 */
	public double getLimite() {
		return limite;
	}

	/**
	 * @param limite
	 *            the limite to set
	 */
	public void setLimite(double limite) {
		this.limite = this.getSaldo() + this.getValorLimite();
	}

	/**
	 * @return the saldo
	 */
	public double getSaldo() {
		return saldo;
	}

	/**
	 * @param saldo
	 *            the saldo to set
	 */
	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}

	/**
	 * @return the valorLimite
	 */
	public double getValorLimite() {
		return valorLimite;
	}

	/**
	 * @param valorLimite
	 *            the valorLimite to set
	 */
	public void setValorLimite(double valor) {
		this.valorLimite = valor;
	};

	public final void sacar(double valor) {
		this.setSaldo(getSaldo() - valor);
	}

	public final void depositar(double valor) {
		this.setSaldo(getSaldo() + valor);
	}

}
