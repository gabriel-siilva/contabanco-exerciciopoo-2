/**
 * 6 de mar de 2018
 */
package br.cesed.si.app;

/**
 * @author aluno
 *
 */
public final class ContaPoupanca extends Conta {
	
	public ContaPoupanca(int agencia, int conta, String titular) {
		super(agencia, conta, titular);
	}

}
