package br.cesed.si.testes;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import br.cesed.si.app.Conta;
import br.cesed.si.app.ContaCorrente;
import br.cesed.si.app.ContaPoupanca;

class ContaTest {
	
	private ContaCorrente conta1;
	
	@BeforeEach
	void setUp() throws Exception {
		conta1 = new ContaCorrente(1,1,"titular1");
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@DisplayName("Teste de dep�sito")
	@Test
	void depositarTest() {
		assertEquals(100, conta1.depositar(100));
	}
	
	@DisplayName("Teste de saque")
	@Test
	void sacarTest() {
	}

}
